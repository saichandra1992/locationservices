//
//  locationTrack.swift
//  LocationServices
//
//  Created by devabhakthuni nomith sai chandra on 8/9/21.
//

import Foundation
import UIKit
import CoreLocation



public let LocationTracking = LocationTrack.shared
public class LocationTrack: NSObject {//, CLLocationManagerDelegate {
    
    /// Shared instance.
    public static let shared = LocationTrack()
    
    var locationManager: CLLocationManager?
    
    var batteryLevel: Float {
        return UIDevice.current.batteryLevel
    }
    
    public func startLocation() {
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        
        self.locationManager?.requestAlwaysAuthorization()
        
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager?.distanceFilter = kCLLocationAccuracyNearestTenMeters
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBestForNavigation
    }
    
}

extension LocationTrack: CLLocationManagerDelegate {

  public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(" batter level information: ", batteryLevel*100)
        print("manager.location?.coordinate.latitude: ", manager.location?.coordinate.latitude)
        print("manager.location?.coordinate.longitude: ", manager.location?.coordinate.longitude)
        if status == .authorizedAlways {
            print("authorizedAlways: ", status.rawValue)
        } else if status == .authorizedWhenInUse {
            print("authorizedWhenInUse: ", status.rawValue)
        } else if status == .denied {
            print("denied: ", status.rawValue)
        } else if status == .notDetermined {
            print("notDetermined: ", status.rawValue)
        } else if status == .restricted {
            print("restricted: ", status.rawValue)
        } else {
            print(" did not select any option")
        }
    }
}
