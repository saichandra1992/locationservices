# LocationServices

[![CI Status](https://img.shields.io/travis/saichandra/LocationServices.svg?style=flat)](https://travis-ci.org/saichandra/LocationServices)
[![Version](https://img.shields.io/cocoapods/v/LocationServices.svg?style=flat)](https://cocoapods.org/pods/LocationServices)
[![License](https://img.shields.io/cocoapods/l/LocationServices.svg?style=flat)](https://cocoapods.org/pods/LocationServices)
[![Platform](https://img.shields.io/cocoapods/p/LocationServices.svg?style=flat)](https://cocoapods.org/pods/LocationServices)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LocationServices is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LocationServices'
```

## Author

saichandra, saidevabhakthuni23@gmail.com

## License

LocationServices is available under the MIT license. See the LICENSE file for more info.
